import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class SessionModel {
  static void save(String iduser) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('id_user', iduser);
    preferences.setBool('status', true);
  }

  Future<String> getId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString('id_user') ?? 'null Id_User';
  }

  Future<bool> getStatusLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getBool('status') ?? false;
  }
}
