import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class GetPesananModel {
  String id;
  String nama_servis;
  String id_main_sub_servis;
  String nama_sub_servis;
  String id_main_kendaraan_tipe;
  String nama_kendaraan;
  String id_user;
  String nama_lengkap;
  String waktu_servis;
  String status;
  String catatan_tambahan;

  GetPesananModel(
      {this.id,
      this.nama_servis,
      this.id_main_sub_servis,
      this.nama_sub_servis,
      this.id_main_kendaraan_tipe,
      this.nama_kendaraan,
      this.id_user,
      this.nama_lengkap,
      this.waktu_servis,
      this.status,
      this.catatan_tambahan});

  factory GetPesananModel.createPesanan(Map<String, dynamic> obj) {
    return GetPesananModel(
      id: obj['id'],
      nama_servis: obj['nama_servis'],
      id_main_sub_servis: obj['id_main_sub_servis'],
      nama_sub_servis: obj['nama_sub_servis'],
      id_main_kendaraan_tipe: obj['id_main_kendaraan_tipe'],
      nama_kendaraan: obj['nama_tipe'],
      id_user: obj['id_user'],
      nama_lengkap: obj['nama_lengkap'],
      waktu_servis: obj['waktu_servis'],
      status: obj['status'],
      catatan_tambahan: obj['catatan_tambahan'],
    );
  }

  static Future<List<GetPesananModel>> getPesanan(String idmitra) async {
    var response = await http.get(
      "http://api.otobenk.com/api/mitra/listorder/id_mitra/2",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'apikey': 'otobenk',
      },
    );

    final jsonObj = json.decode(response.body);
    Iterable list = jsonObj;

    return list.map((model) => GetPesananModel.createPesanan(model)).toList();

    // List<GetPesananModel> gta = jsonObj
    //     .map(
    //       (dynamic item) => GetPesananModel.createPesanan(item),
    //     )
    //     .toList();
  }
}
