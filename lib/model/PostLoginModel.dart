import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class PostLogin {
  String iduser;
  String email;
  String level;
  bool verified;
  bool status;

  PostLogin({this.iduser, this.email, this.level, this.verified, this.status});

  factory PostLogin.fromJson(Map<String, dynamic> json) {
    return PostLogin(
      status: true,
      iduser: json['id_user'],
      email: json['email'],
      level: json['level'],
      verified: json['verified'],
    );
  }

  factory PostLogin.failJson(Map<String, dynamic> objjson) {
    return PostLogin(status: objjson['status']);
  }

  static Future<PostLogin> action(String email, String password) async {
    // final http.Response
    var response = await http.post(
      "http://api.otobenk.com/api/user/login",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'apikey': 'otobenk',
      },
      body: jsonEncode(<String, String>{
        'email': email,
        'password': password,
      }),
    );

    var jsonObjs = json.decode(response.body);
    var jsonObj = json.decode(response.body)['data'];

    if (response.statusCode == 200) {
      return PostLogin.fromJson(jsonObj);
    } else {
      return PostLogin.failJson(jsonObjs);
    }
  }
}
