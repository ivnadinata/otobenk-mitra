import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otobenk_user/model/GetPesananModel.dart';
import 'package:otobenk_user/screens/DetailOrderPage.dart';
import 'package:otobenk_user/util/color_palette.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:grouped_list/grouped_list.dart';

void main() {
  runApp(HomePage());
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedTabIndex = 0;
  List data;

  Future<String> getData() async {
    http.Response response = await http.get(
        Uri.encodeFull("http://api.otobenk.com/api/mitra/listorder/id_mitra/2"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'apikey': 'otobenk',
        });
    setState(() {
      data = json.decode(response.body);
    });
    return "Success!";
  }

  void _onNavbarTapped(int index) {
    setState(() {
      _selectedTabIndex = index;

      if (index == 0) {
        this.getData();
        print('homepage yaa !');
      } else if (index == 1) {
        print('Profile ini Yaa !');
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    final _listpage = <Widget>[
      // buildListPesanan(),
      GroupedListView<dynamic, String>(
        groupBy: (element) => element['status'],
        elements: (data == null) ? 0 : data,
        order: GroupedListOrder.ASC,
        useStickyGroupSeparators: false,
        groupSeparatorBuilder: (String value) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            (() {
              if (value == '1') {
                return "Pesanan terbaru";
              } else if (value == '2') {
                return "Proses pengerjaan";
              } else if (value == '3') {
                return "Riwayat pesanan";
              }
            })(),
            style: TextStyle(fontWeight: FontWeight.w800, fontSize: 16),
          ),
        ),
        itemBuilder: (c, element) {
          return SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 6,
            child: Container(
              margin: EdgeInsets.all(0),
              padding: EdgeInsets.all(0),
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  splashColor: Colors.grey,
                  onTap: () {
                    print(element["id"].toString());
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DetailOrderPage(),
                      ),
                    );
                  },
                  child: Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(10),
                        child: Icon(
                          Icons.person_outline,
                          color: ColorPallete.dotActiveColor,
                          size: 32.0,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 5, top: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${element['nama_lengkap']}",
                              style: TextStyle(
                                fontWeight: FontWeight.w800,
                                fontSize: 14,
                              ),
                            ),
                            Text(
                              "${element['nama_sub_servis']} ${element['nama_tipe']}",
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text(
                              "12 September 2020",
                              style: TextStyle(
                                fontSize: 10,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
            ),
          );
        },
      ),
      buildProfile(context),
    ];

    final _bottomNavbarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.motorcycle),
        title: Text('Pesanan'),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.account_circle),
        title: Text('Profile'),
      )
    ];

    final _bottomNavbar = BottomNavigationBar(
      elevation: 10,
      items: _bottomNavbarItems,
      currentIndex: _selectedTabIndex,
      selectedItemColor: ColorPallete.dotActiveColor,
      unselectedItemColor: Colors.grey,
      onTap: _onNavbarTapped,
    );

    return Scaffold(
      body: Container(
        child: _listpage[_selectedTabIndex],
      ),
      bottomNavigationBar: _bottomNavbar,
    );
  }

  Container buildProfile(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height / 25),
      child: Column(
        children: <Widget>[
          Image.network(
            'http://otobenk.com/image/bengkel/haha_resize.png',
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Card(
              elevation: 5,
              child: Container(
                margin: EdgeInsets.all(10),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.email,
                      color: ColorPallete.dotActiveColor,
                      size: 30,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20),
                      child: Text(
                        "ivanadinata1111@gmail.com",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Card(
              elevation: 5,
              child: Container(
                margin: EdgeInsets.all(10),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.people,
                      color: ColorPallete.dotActiveColor,
                      size: 30,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20),
                      child: Text(
                        "Ivan Adinata",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Card(
              elevation: 5,
              child: Container(
                margin: EdgeInsets.all(10),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.business_center,
                      color: ColorPallete.dotActiveColor,
                      size: 30,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20),
                      child: Text(
                        "Pemilik Bengkel Haha",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w500),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
