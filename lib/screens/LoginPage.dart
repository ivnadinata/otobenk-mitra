import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otobenk_user/model/PostLoginModel.dart';
import 'package:otobenk_user/model/SessionModel.dart';
import 'package:otobenk_user/screens/HomePage.dart';
import 'package:otobenk_user/util/color_palette.dart';

void main() {
  runApp(LoginPage());
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController editingEmailController = TextEditingController();

  TextEditingController editingPasswordController = TextEditingController();

  PostLogin postLogin = null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height / 15.0),
        child: ListView(
          children: <Widget>[
            Container(
              child: Center(
                child: Image(image: AssetImage('images/logo_test.png')),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 30.0),
              child: Center(
                child: Text(
                  "Aplikasi Mitra Otobenk",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: MediaQuery.of(context).size.width / 10,
                left: MediaQuery.of(context).size.width / 15,
                right: MediaQuery.of(context).size.width / 15,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  emailTextField(),
                  spaceColumn(),
                  passwordTextField(),
                  spaceColumn(),
                  spaceColumn(),
                  buttonLogin(context),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  SizedBox spaceColumn() {
    return SizedBox(
      height: 25,
      child: Material(
        color: Colors.white,
      ),
    );
  }

  SizedBox buttonLogin(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 1,
      height: 45,
      child: Container(
        child: Material(
          borderRadius: BorderRadius.circular(50),
          color: Colors.transparent,
          child: InkWell(
            borderRadius: BorderRadius.circular(50),
            splashColor: ColorPallete.dotColor,
            onTap: () {
              PostLogin.action(
                editingEmailController.text,
                editingPasswordController.text,
              ).then((value) {
                postLogin = value;

                if (value.status) {
                  if (value.level == '2') {
                    SessionModel.save(value.iduser.toString());
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) {
                      return HomePage();
                    }));
                  } else {}
                } else {}

                setState(() {});
              });
            },
            child: Center(
              child: Text(
                "Masuk",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
        ),
        decoration: BoxDecoration(
            color: ColorPallete.dotActiveColor,
            borderRadius: BorderRadius.circular(50)),
      ),
    );
  }

  TextField emailTextField() {
    return TextField(
      keyboardType: TextInputType.emailAddress,
      style: TextStyle(fontSize: 12),
      decoration: InputDecoration(
        filled: false,
        fillColor: ColorPallete.dotColor,
        prefixIcon: Icon(
          Icons.email,
          color: ColorPallete.dotActiveColor,
        ),
        border: UnderlineInputBorder(),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: ColorPallete.dotActiveColor),
        ),
        hintText: "masukan email anda ...",
        labelText: "Email",
        hintStyle: TextStyle(fontSize: 12),
        labelStyle: TextStyle(fontSize: 12, color: ColorPallete.dotActiveColor),
      ),
      controller: editingEmailController,
    );
  }

  TextField passwordTextField() {
    return TextField(
      obscureText: true,
      style: TextStyle(fontSize: 12),
      decoration: InputDecoration(
        filled: false,
        fillColor: ColorPallete.dotColor,
        prefixIcon: Icon(
          Icons.vpn_key,
          color: ColorPallete.dotActiveColor,
        ),
        border: UnderlineInputBorder(),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: ColorPallete.dotActiveColor),
        ),
        hintText: "masukan katasandi anda ...",
        labelText: "Katasandi",
        hintStyle: TextStyle(fontSize: 12),
        labelStyle: TextStyle(fontSize: 12, color: ColorPallete.dotActiveColor),
      ),
      controller: editingPasswordController,
    );
  }
}
